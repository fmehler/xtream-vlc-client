package xtreamclient.util

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
fun <T> Deferred<T>.completedOrNull():T? {
    if(this.isCompleted) {
        return this.getCompleted()
    } else {
        return null
    }
}