package xtreamclient.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

data class UserInfo(
    val username:String,
    val password:String,
    val auth:Int,
    val status:String,
    @SerializedName("allowed_output_formats")
    val allowedOutputFormats:List<String>)
data class ServerInfo(val url:String, val timezone:String)

data class PlayerApi(
    @SerializedName("user_info")
    val userInfo: UserInfo,
    @SerializedName("server_info")
    val serverInfo: ServerInfo
)

data class MediaCategory(
    @SerializedName("category_id")
    val categoryId:String,
    @SerializedName("category_name")
    val categoryName:String,
    @SerializedName("parent_id")
    val parentId:Int)


val dtf = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm")

typealias Movie = LiveStream
data class LiveStream(
    val num:Int,
    val name:String,
    @SerializedName("stream_id")
    val id:Int,
    @SerializedName("stream_icon")
    val icon:String,
    @SerializedName("epg_channel_id")
    val epgChannelId:String,
    @SerializedName("category_id")
    val categoryId: String,
    @SerializedName("tv_archive")
    val hasArchive: Int,
    @SerializedName("tv_archive_duration")
    val maxArchiveLength: String) {

    fun getEarliestDateString():String {
        return LocalDateTime.now().minusDays(maxArchiveLength.toLongOrNull()?:0).format(dtf)
    }

    fun getLatestDateString():String {
        return LocalDateTime.now().format(dtf)
    }
}

data class Series(
    val num: Int,
    val name: String,
    @SerializedName("series_id")
    val seriesId: Int,
    val cover:String?,
    val plot:String?
    //TODO: All data
)


data class SeriesOverview(
    val seasons: List<Season>,
    @JsonAdapter(EpisodeSeasonAdapter::class)
    val episodes: List<EpisodeSeason>
)

data class Season(
    @SerializedName("air_date")
    val airDate:String,
    @SerializedName("episode_count")
    val episodeCount:Int,
    @SerializedName("id")
    val seasonId:Int,
    val name:String,
    @SerializedName("season_number")
    val seasonNumber:Int
)

data class EpisodeSeason(
    val SeasonName:String,
    val episodes: List<Episode>
)

data class Episode(
    val id:Int,
    @SerializedName("episode_num")
    val episodeNum:Int,
    val title:String
)


class EpisodeSeasonAdapter :TypeAdapter<List<EpisodeSeason>>() {
    override fun write(out: JsonWriter?, value: List<EpisodeSeason>?) {
        return //Not needed
    }

    override fun read(jr: JsonReader?): List<EpisodeSeason> {
        if(jr!=null) {
            jr.beginObject()
            val episodeSeasonList = mutableListOf<EpisodeSeason>()
            while (jr.hasNext()) {
                val seasonName = jr.nextName()
                jr.beginArray()
                val episodeList = mutableListOf<Episode>()
                while (jr.hasNext()) {
                    //Episodes
                    episodeList += GsonBuilder().create().fromJson<Episode>(jr,Episode::class.java)
                }
                episodeSeasonList += EpisodeSeason(seasonName,episodeList)
                jr.endArray()
            }
            jr.endObject()
            return episodeSeasonList
        }
        return listOf()
    }

}



