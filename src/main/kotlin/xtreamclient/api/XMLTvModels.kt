package xtreamclient.api

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
data class TV (
    @JsonProperty("generator-info-name")
    val generator:String,
    @JsonProperty("generator-info-url")
    val infoUrl:String,

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("programme")
    val programmes:List<Programme>,
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("channel")
    val channels:List<Channel>
)
@JsonIgnoreProperties(ignoreUnknown = true)
data class Channel (
    val id:String?,
    @JsonProperty("display-name")
    val name:String?
)
val programme_dtf = DateTimeFormatter.ofPattern("uuuuMMddHHmmss Z")
val outputFormatter = DateTimeFormatter.ofPattern("HH:mm")

data class Programme(
    val start:String,
    val stop:String,
    val channel:String,
    val title:String,
    val desc:String?
) {
    val startDate: ZonedDateTime
        get() = ZonedDateTime.from(programme_dtf.parse(start))
    val endDate:ZonedDateTime
        get() = ZonedDateTime.from(programme_dtf.parse(stop))

    val startTime: String
        get() = outputFormatter.format(startDate.withZoneSameInstant(ZoneId.systemDefault()))
    val endTime: String
        get() = outputFormatter.format(endDate.withZoneSameInstant(ZoneId.systemDefault()))
}


data class LiveStreamWithCurrentProgramme(
    val liveStream: LiveStream,
    val currentProgramme: Programme?,
    val nextPrograme: Programme?
)

/*
data class TimeshiftProgramme(
    val streamId:String?,
    val programme:Programme,
    val
)*/
