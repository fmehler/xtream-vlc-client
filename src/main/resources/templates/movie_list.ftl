<#-- @ftlvariable name="streams" type="java.util.List<xtreamclient.api.Movie>" -->
<#-- @ftlvariable name="server" type="Integer" -->
<#-- @ftlvariable name="servername" type="String" -->
<#-- @ftlvariable name="categoryname" type="String" -->
<html lang="en">
<head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="/static/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" href="/static/bootstrap-datetimepicker.min.css" >
<script>

        function startStream(id) {
                $.ajax({
                        url:"/${server}/play-movie/"+id,
                        success: function (result) {
                                $('.alert').innerText = "Sucessfully started Stream"
                                $('.alert').alert()
                        }
                })
        }



        function restartVLC() {
                $.ajax({
                        url:"/api/restartVLC",
                        success:function (result) {

                        }
                })
        }
</script>
</head>
<body>
<h3>Streams</h3>

<nav aria-label="breadcrumb">
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/${server}/">Server ${servername}</a></li>
                <li class="breadcrumb-item active" aria-current="page">Category: ${categoryname}</li>
        </ol>
</nav>
<div style="position: fixed;background: white; right: 0px; top: 30px;width:150px; padding:5px">
<a href="javascript:restartVLC()">Restart VLC</a>
</div>
<table class="table">
        <thead>
        <tr>
                <td></td>
                <td>Name</td>
                <td>Play Stream</td>
                <td>Get Stream link</td>

        </tr>
        </thead>
    <#list streams as stream>
            <tr>
                    <td><img src="${(stream.icon)!"null"}" style="height:1.5rem" /> </td>
                    <td>${stream.name}</td>
                    <td><a href="javascript:startStream(${stream.id?c})" >Play Stream</a></td>

                    <td></td>

            </tr>

    </#list>
</table>
<div class="alert alert-success" role="alert">
        On Scucces
</div>
</body>
</html>