<#-- @ftlvariable name="categories" type="java.util.List<xtreamclient.api.MediaCategory>" -->
<#-- @ftlvariable name="movieCats" type="java.util.List<xtreamclient.api.MediaCategory>" -->
<#-- @ftlvariable name="series" type="java.util.List<xtreamclient.api.Series>" -->
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<h3>Categories</h3>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <h5>Live</h5>
            <ul>
                <#list categories as cat>
                    <li><a href="category/${cat.categoryId}/">${cat.categoryName}</a></li>
                </#list>
            </ul>
        </div>
        <div class="col-sm">

            <h5>Movies</h5>
            <ul>
                <#list movieCats as cat>
                    <li><a href="movie-category/${cat.categoryId}/">${cat.categoryName}</a></li>
                </#list>
            </ul>
        </div>
        <div class="col-sm">
            <h5>Series</h5>
            <ul>
                <#list series as serie>
                    <li><a href="series-category/${serie.seriesId}">${serie.name}</a></li>
                </#list>
            </ul>
        </div>
    </div>
</div>
</body>
</html>