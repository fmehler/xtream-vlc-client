<#-- @ftlvariable name="streams" type="java.util.List<xtreamclient.api.LiveStreamWithCurrentProgramme>" -->
<#-- @ftlvariable name="server" type="Integer" -->
<#-- @ftlvariable name="servername" type="String" -->
<#-- @ftlvariable name="categoryname" type="String" -->
<html lang="en">
<head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="/static/bootstrap-datetimepicker.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js"></script>
        <link rel="stylesheet" href="/static/bootstrap-datetimepicker.min.css" >
        <title>${categoryname} on ${servername}</title>
<script>

        function startStream(id) {
                $.ajax({
                        url:"/${server}/play/"+id,
                        success: function (result) {
                                $('.alert').innerText = "Sucessfully started Stream"
                                $('.alert').alert()
                        }
                })
        }

        function startTimeshiftStream(id) {
                var startTime = $('#datetime-stream-'+id).val();
                var duration =$('#duration-stream-'+id).val();
                $.ajax({
                        url:"/${server}/play-timeshift/"+id+"/"+startTime+"/"+duration,
                        success:function (result) {

                        }
                })
        }

        function restartVLC() {
                $.ajax({
                        url:"/api/restartVLC",
                        success:function (result) {

                        }
                })
        }
</script>
</head>
<body>
<h3>Streams</h3>

<nav aria-label="breadcrumb">
        <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/${server}/">Server ${servername}</a></li>
                <li class="breadcrumb-item active" aria-current="page">Category: ${categoryname}</li>
        </ol>
</nav>
<div style="position: fixed;background: white; right: 0px; top: 30px;width:150px; padding:5px">
<a href="javascript:restartVLC()">Restart VLC</a><br>
</div>
<table class="table">
        <thead>
        <tr>
                <td></td>
                <td>Name</td>
                <td>Play Stream</td>
                <td>Play Timeshift</td>
                <td>Get Stream link</td>
                <td>Current Show</td>
        </tr>
        </thead>
    <#list streams as stream>
            <tr>
                    <td><img class="lazyload" src="${stream.liveStream.icon}" style="height:1.5rem" /> </td>
                    <td>${stream.liveStream.name}</td>
                    <td><a href="javascript:startStream(${stream.liveStream.id?c})" >Play Stream</a></td>
                    <td>
                            <#if stream.liveStream.hasArchive == 1>
                                    <button class="btn btn-secondary btn-sm"
                                            type="button"
                                            data-toggle="collapse"
                                            data-target="#collapseStream${stream.liveStream.id?c}">Timeshift vorhanden</button>
                                    <div class="collapse" id="collapseStream${stream.liveStream.id?c}">
                                            <div class="card card-body">
                                                    <label for="datetime-stream-${stream.liveStream.id?c}">time:</label><br>
                                                    <input size="16" type="text" value="" readonly class="form_datetime"
                                                    id="datetime-stream-${stream.liveStream.id?c}">
                                                    <label for="duration-stream-${stream.liveStream.id?c}">Duration in min:</label>
                                                    <input type="number" value="30" id="duration-stream-${stream.liveStream.id?c}" />

                                                    <a href="javascript:startTimeshiftStream(${stream.liveStream.id?c})" class="btn btn-primary btn-sm"
                                                            type="button">Watch</a>
                                            </div>
                                            <script type="text/javascript">
                                                    $("#datetime-stream-${stream.liveStream.id?c}").datetimepicker({
                                                            format: 'yyyy-mm-dd:hh-ii',
                                                            startDate: "${stream.liveStream.getEarliestDateString()}",
                                                            endDate: "${stream.liveStream.getLatestDateString()}"
                                                    });
                                            </script>
                                    </div>

                            </#if>
                    </td>
                    <td></td>
                    <td>Current: <#if stream.currentProgramme??>
                                    ${stream.currentProgramme.startTime} - ${stream.currentProgramme.endTime}
                                    ${stream.currentProgramme.title!"No EPG"}
                            </#if><br>
                        Following: <#if stream.nextPrograme??>
                                    ${stream.nextPrograme.startTime} - ${stream.nextPrograme.endTime}
                                    ${stream.nextPrograme.title!"No EPG"}
                            </#if>
                    </td>
            </tr>

    </#list>
</table>
<div class="alert alert-success" role="alert">
        On Scucces
</div>
<div class="modal fade" id="epgmodal" tabindex="-1" role="dialog" aria-labelledby="epgmodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
                <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title" id="epgmodallabel">EPG for $$TV-Station$$</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="close">
                                        <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div class="modal-body">
                                EPG will be here
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                </div>
        </div>
</div>
<script>
        lazyload();
</script>
</body>
</html>