<#-- @ftlvariable name="server" type="xtreamclient.NamedServerProperties" -->
<#-- @ftlvariable name="id" type="Integer" -->
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">

    Really delete ${server.properties.username}@${server.properties.hostname} from this servers configuration?
    <br>
    <a class="btn btn-danger" href="/${id}/remove/confirm">Confirm</a> <a class="btn btn-secondary" href="/">Cancel</a>
</div>
</body>
</html>