<#-- @ftlvariable name="series" type="xtreamclient.api.SeriesOverview" -->
<#-- @ftlvariable name="server" type="Integer" -->
<#-- @ftlvariable name="servername" type="String" -->
<#-- @ftlvariable name="categoryname" type="String" -->
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="/static/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="/static/bootstrap-datetimepicker.min.css" >
    <script>

        function startStream(id) {
            $.ajax({
                url:"/${server}/play-series/"+id,
                success: function (result) {
                    $('.alert').innerText = "Sucessfully started Stream"
                    $('.alert').alert()
                }
            })
        }


        function restartVLC() {
            $.ajax({
                url:"/api/restartVLC",
                success:function (result) {

                }
            })
        }
    </script>
</head>
<body>
<h3>Series</h3>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/${server}/">Server ${servername}</a></li>
        <li class="breadcrumb-item active" aria-current="page">Series: ${categoryname}</li>
    </ol>
</nav>
<div class="container">
    <div class="d-flex flex-wrap justify-content-center">
        <#list series.episodes as season>
            <div class="p-2">
                <h4>Season ${season.seasonName}</h4>
                <div class="list-group" style="width: 20rem;">
                    <#list season.episodes as episode>
                        <a href="javascript:startStream(${episode.id?c})" class="list-group-item list-group-item-action">${season.seasonName} - ${episode.episodeNum} ${episode.title}</a>
                    </#list>
                </div>
            </div>
        </#list>
    </div>
</div>
</body>
</html>