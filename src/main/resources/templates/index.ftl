<#-- @ftlvariable name="servers" type="List<xtreamclient.NamedServerProperties>" -->
<html>
<head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">

<table class="table">
    <#list servers as server>
        <tr>
                <td><a class="btn btn-primary" href="/${server.id}/">${server.properties.hostname}</a></td>
                <td class="text-right"><a class="btn btn-danger" href="/${server.id}/remove">Delete</a></td>

        </tr>
    </#list>
</table>


        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addServerModal">
        Add Server
        </button>
        <!-- Modal -->
        <div class="modal fade" id="addServerModal" tabindex="-1" role="dialog" aria-labelledby="addServerModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add Server</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                        </button>
                                </div>
                                <form action="/addServer" method="post">
                                <div class="modal-body">
                                        <div class="form-group">
                                                <label for="newServerURL">Playlist URL:</label>
                                                <input type="text" class="form-control" name="newServerURL" id="newServerURL"
                                                       aria-describedby="emailHelp" placeholder="http://blablabla.com:8080/get.php?...">
                                        </div>
                                </div>
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Send</button>
                                </div>
                                </form>
                        </div>
                </div>
        </div>

        <a href="/reloadConfig" type="button" class="btn btn-secondary">
                Reload Configuration
        </a>
</div>
</body>
</html>